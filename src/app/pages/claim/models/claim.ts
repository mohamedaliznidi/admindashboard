export interface Claim {
    idclaim : number;
    description : string;
	subject_claim : string;
	state : boolean;
}